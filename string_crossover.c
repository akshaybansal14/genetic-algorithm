#include <stdio.h>
#include <stdlib.h>
#include "string_crossover.h"
#include "defs.h"

void string_crossover(int **Q){

	int temp;
	int alpha, i, k;
	float pr;

	alpha = rand()%(STRING_LEN-1) + 1;
	
	for (i = 0; i < M/2; i++){
		pr = (rand()%10000)/10000;
		if (pr < P_CROSS){
			for (k = alpha; k < STRING_LEN; k++){
				temp = Q[i][k]; Q[i][k] = Q[i+1][k]; Q[i+1][k] = temp;
			}
		}
	}

	return;
}


