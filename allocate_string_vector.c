#include <stdio.h>
#include <stdlib.h>
#include "allocate_string_vector.h"
#include "defs.h"

int **allocate_string_vector(){

	int **S;
	int i;

	S = (int**)calloc(M,sizeof(int*));
	if(S == NULL)
		ERR_MESG("\npopulation: out of memory\n");
	for (i = 0; i < M; i++){
		S[i] = (int*)calloc(STRING_LEN, sizeof(int));
		if (S[i] == NULL)
			ERR_MESG("\npopulation: out of memory\n")
	}

	return(S);
}
