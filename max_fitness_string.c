#include <stdio.h>
#include "max_fitness_string.h"
#include "string_eval.h"
#include "defs.h"

int max_fitness_string(int **Q, float *F){

	float max = 0, val;
	int index_max, i;


	for (i = 0; i < M; i++){
		if((val = string_eval(Q[i])) > max){
			max = val;
			index_max = i;
		}
		F[i] = val;
	}
	return(index_max);

}


