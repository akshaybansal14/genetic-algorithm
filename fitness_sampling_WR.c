#include <stdio.h>
#include <stdlib.h>
#include "fitness_sampling_WR.h"
#include "defs.h"

void fitness_sampling_WR(int **Q, int **T, float *F){

	float sum=0, pr;
	int low, high, mid, i, j;

	for (i = 0; i < M; i++){
		sum += F[i];
	}
	F[0] = F[0]/sum;
	for (i = 1; i < M; i++)
		F[i] = F[i-1] + F[i]/sum; // normalizing fitness value between 0 and 1

	/* To perform binary search on the set of fitness values in order to select based on 'roulette wheel' scheme */
	for (i = 0; i < M; i++){
		pr = (rand()%10000)/10000;
		if (pr < F[0])
			high = 0;
		else{
			low = 0; high = M-1;
			while (high > low + 1){
				mid = (low + high)/2;
				if (pr < F[mid])
					high = mid;
				else
					low = mid;
			}
		}
		for (j = 0; j < STRING_LEN; j++)
			T[i][j] = Q[high][j];
	}

	return;
}

