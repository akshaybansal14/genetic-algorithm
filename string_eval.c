#include <stdio.h>
#include <math.h>
#include "string_eval.h"
#include "defs.h"

float string_eval(int *S){

	int r, k;
	float x = 0, val;

	for(k = 0; k < STRING_LEN; k++)
		x += pow(2, 4-k) * S[k];
	r = floor(x/(2*PI));
	
	if(r < 5)
		val = (r+1)*sin(x) + 6;
	else
		val = sin(x) + 6;

	return(val);
}
