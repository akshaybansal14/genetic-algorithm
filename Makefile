CC = gcc
CFLAGS = -c -Wall
LDFLAGS = -lm
OBJECTS = main.o fitness_sampling_WR.o string_crossover.o string_mutation.o string_eval.o max_fitness_string.o allocate_string_vector.o free2D.o 


prog: $(OBJECTS)
	$(CC) $(OBJECTS) -o genetic_algorithm $(LDFLAGS)

main.o : main.c defs.h allocate_string_vector.h string_eval.h fitness_sampling_WR.h max_fitness_string.h string_crossover.h string_mutation.h free2D.h
	$(CC) $(CFLAGS) main.c 

fitness_sampling_WR.o : fitness_sampling_WR.c fitness_sampling_WR.h defs.h 
	$(CC) $(CFLAGS) fitness_sampling_WR.c

string_crossover.o : string_crossover.c string_crossover.h defs.h
	$(CC) $(CFLAGS) string_crossover.c

string_mutation.o : string_mutation.c string_mutation.h defs.h
	$(CC) $(CFLAGS) string_mutation.c

string_eval.o : string_eval.c string_eval.h defs.h
	$(CC) $(CFLAGS) string_eval.c
 
max_fitness_string.o : max_fitness_string.c max_fitness_string.h string_eval.h defs.h
	$(CC) $(CFLAGS) max_fitness_string.c
 
allocate_string_vector.o : allocate_string_vector.c allocate_string_vector.h defs.h
	$(CC) $(CFLAGS) allocate_string_vector.c
 
free2D.o : free2D.c free2D.h defs.h
	$(CC) $(CFLAGS) free2D.c

clean:
	rm -f $(OBJECTS) 
