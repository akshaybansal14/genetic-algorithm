#define M 10
#define N_ITER 450
#define N_POPULATIONS 10
#define N_EXEC 10
#define P_CROSS 0.75
#define P_MUT 0.85
#define STRING_LEN 15
#define PI 3.141592
#define ERR_MESG(x) { perror(x); exit(1); }


