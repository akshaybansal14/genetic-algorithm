
#include <stdio.h>
#include <stdlib.h>
#include "string_mutation.h"
#include "defs.h"

void string_mutation(int **Q){

	int i, j;
	float pr;

	for (i = 0; i < M; i++){
		for (j = 0; j < STRING_LEN; j++){
			pr = (rand()%10000)/10000;
			if(pr < P_MUT)	// mutate bit if pr < probability of mutation
				Q[i][j] = 1 - Q[i][j];
		}
	}

	return;
}
