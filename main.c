/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Name: Akshay Bansal
Roll No: CS1520
Program Description: Implement Genetic Algorithm (Elitist) model for a given function
Date of Submission:
Date of Deadline:
Acknowledgment: The C Programming Language (K&R)
---------------------------------------------------------------------------------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "allocate_string_vector.h"
#include "string_eval.h"
#include "fitness_sampling_WR.h"
#include "max_fitness_string.h"
#include "string_crossover.h"
#include "string_mutation.h"
#include "free2D.h"
#include "defs.h"

int main(){

	srand(time(NULL));
	
	int i, j, l, m, n, index_max, index_max_Q3, random_index;
	int **Q, **Q_copy, **T, **temp, *S_max, index_max_pop, flag;
	float Q0_max, Q3_max, *F, sum = 0, max_pop, max_val = 0;
	
	Q = allocate_string_vector();
	Q_copy = allocate_string_vector(); 	// working copy 1
	T = allocate_string_vector();		// working copy 2 (useful when roulette wheel selection is performed to prevent back and forth string copying)

	F = (float*)calloc(M, sizeof(float)); // array to store fitness values
	if (F == NULL)
		ERR_MESG("\nGA: out of memory\n")
	
	/* String corrsponding to maximum fitness value */
	S_max = (int*)calloc(STRING_LEN, sizeof(int)); // string with maximum fitness value in population Q0
	if(S_max == NULL)
		ERR_MESG("\nGA: out of memory\n");

	for (l = 0; l < N_POPULATIONS; l++){ 

		/* Random Sampling with replacement */
		for (i = 0; i < M; i++){
			for (j = 0; j < STRING_LEN; j++){
				Q[i][j] = rand()%2;
			}
		}
		index_max_pop = max_fitness_string(Q,F); 
		max_pop = string_eval(Q[index_max_pop]);

		for (m = 0; m < N_EXEC; m++){

			/* To rerun the genetic evolution process with same population */
			for (i = 0; i < M; i++){
				for (j = 0; j < STRING_LEN; j++){
					Q_copy[i][j] = Q[i][j];
				}
			}
			index_max = index_max_pop;
			Q0_max = max_pop;	

			flag = 1; // to keep track the string corresponding to max. fitness value changes in population sets {Q0, Q1, Q2, Q3}							
			for (n = 0; n < N_ITER; n++){

				if(flag == 1){
					for (j = 0; j < STRING_LEN; j++)
						S_max[j] = Q_copy[index_max][j];
				}			

				fitness_sampling_WR(Q_copy, T, F); 

				temp = Q_copy; Q_copy = T; T = temp; // change working copy as selected (orderly) strings are stored in T
				string_crossover(Q_copy);
				string_mutation(Q_copy);

				/* Elitist Approach to form evolved population */
				index_max_Q3 = max_fitness_string(Q_copy, F);
				Q3_max = string_eval(Q_copy[index_max_Q3]);
				if(Q3_max < Q0_max){
					random_index = rand() % M; // random string that is to be deleted from Q3
					index_max = random_index;
					for(j = 0; j < STRING_LEN; j++)
						Q_copy[random_index][j] = S_max[j];
					F[random_index] = Q0_max;
					flag = 0; // denotes current population max. string is smaller (fitness wise) than that of Q0
				}
				else{
					index_max = index_max_Q3;
					Q0_max = Q3_max;
					flag = 1; // denotes the string with maximum fitness value in current population is higher than that of Q0
				}
			}
			if(Q0_max > max_val)
				max_val = Q0_max;
			sum += Q0_max;
		}
	}
	printf("MAX = %f, AVG = %f\n",max_val,sum/(N_POPULATIONS*N_EXEC));
	free2D(Q); free2D(Q_copy); free2D(T);
	free(S_max); free(F);

	return(0);

}

